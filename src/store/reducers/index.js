import { combineReducers } from "redux";
import { carsReducers } from "./CarsReducers";

const reducers = combineReducers({
  cars: carsReducers,
});

export default reducers;
