import { LOAD_CARS } from "../actions";

const initialState = {
    cars: [],
}

export function carsReducers(state ={data: []}, action) {
    switch (action.type) {
      case LOAD_CARS:
        //   console.log("ini paylod",action.payload)
        return { ...state, data: action.payload };
     
      default:
        return state;
    }
  }
  