import logo from './logo.svg';
// import './App.css';
import Button from './components/Button';
import { Link,Route,Routes} from "react-router-dom"
import Home from './page/Home';

import Cars from './page/Cars';
// import Album from './page/Album';
// import Gallery from './page/Gallery';
// import Post from './page/Post';
// import User from './page/User';
// import Dashboard from './page/dashboard';

import './index.css';
import Counter from './components/Counter';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
function App() {
  return (
    <div className="App">
     {/* <ul>
       <li><Link to={'/'}>home</Link></li>
       <li><Link to={'/post'}>post</Link></li>
       <li><Link to={'/album'}>album</Link></li>
       <li><Link to={'/gallery'}>gallery</Link></li>
       <li><Link to={'/users'}>users</Link></li>
     </ul> */}
      <Routes>
        <Route path='/' element = {<Home />}/>
        <Route path='/cars' element = {<Cars/>}/>

        {/* <Route path='/post' element = {<Post />}/>
   
        <Route path='/users' element = {<User/>}/>
        <Route path='/login' element = {<Login/>}/>
        <Route path='/register' element = {<Register/>}/> */}
      </Routes>
    </div>
  );
}

export default App;
