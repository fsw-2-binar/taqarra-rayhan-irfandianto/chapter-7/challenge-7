import React, {useEffect, useState,Component} from 'react';
import mobil from '../asset/image/img_car.png';
import { connect } from "react-redux";
import { loadCars } from "../store/actions";

import { useDispatch, useSelector } from 'react-redux';


const Cars = () => {


   
    // console.log("ini daate",data1[1].availableAt)
    // console.log("ini getime",new Date(data1[1].availableAt).getTime())
    const [tipedriver,setTipeDriver]=useState('')
    const [tanggal,setTanggal]=useState('')
    const [kapasitas,setKapasitas]=useState(0)
    const handleTipeDriver = (event)=> {
        setTipeDriver(event.target.value)
        
    }

    const handleTanggal = (event) => {
        setTanggal(event.target.value)
        
    }
    const handlekapasitas = (event) => {
        setKapasitas(event.target.value)
        console.log(kapasitas)
    }

    const data1 = useSelector(state => state.cars.data)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadCars(kapasitas,tanggal))
    }, []);
    return (
        <div>
             <>
                 <header>
        

        <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor:'#F1F3FF'}}>
            <div className="container-fluid">
              <a className="navbar-brand" href="#" style={{ backgroundColor:'black', marginLeft: '5%'}}>Navbar</a>
              <button id="tombol-burger"className="btn " type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"><span className="navbar-toggler-icon"></span></button>

            <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div className="offcanvas-header">
                
                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div className="offcanvas-body">
                    <ul>
                        <li><a className="cepet" href="#best-car"> Our Service</a></li>
                        <li><a className="cepet" href="#why-us">Why Us</a></li>
                        <li><a  className="cepet" href="#testimonial">Testimonial</a></li>
                        <li><a className="cepet"href="#faq" >FAQ</a></li>
                        <li><button type="button" className="btn btn-success">Register </button></li>
    
                    </ul>
                </div>
            </div>
            </div>
        </nav>

    </header>
    <main style={{width: '100%'}} >
    <section class="desc">
            
            <div class="container-fluid kotak-deskripsi" >
                <div class="row atas"  >
                    <div class="col-lg-6 col-12 justify-center area-deskripsi" >

                        <div class="teks-deksripsi" >
                            <h1 class="h1 desk1">Sewa Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                            <h3 class="h3 mt-3 desk2">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. 
                                Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</h3>
                        </div>
                        <div class="row">

                            <div class="col-md-7 rent">
                                <a href="/cars"><button type="button" class="btn btn-success" id="sewa">Mulai Sewa Mobil </button></a>
                                
                                
                            </div>
                           
                
                
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-6 col-12 justify-content-center area-gambar">
                       
                        <img src={mobil} class="img-fluid float-end area-mobil" />
                       
                    </div>
                    

                </div>
                
                
            </div>  

            
        

        
        


    </section>

    <section class="form-cari">

            <div class="" id="startMenuMulaiSewa">
                <div class="container">
                   
                    <div class="row align-items-start menu-mulai-sewa-deskripsi">
                        <div class="col-3">
                            <p>Tipe Driver</p>
                        </div>
                        <div class="col-2">
                            <p>Tanggal</p>
                        </div>
                        <div class="col-2">
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div class="col-3">
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                    </div>
                   
                    <div class="row align-items-center menu-mulai-sewa-konten">
                        <div class="col-3">
                            <select name="cars" id="tipeDriver" class="form-select menus" onChange={(e)=> handleTipeDriver(e)}>
                                <option value="tipeDriver">Pilih Tipe Driver</option>
                                <option value="denganSopir">Dengan Sopir</option>
                                <option value="tanpaSopir">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <input type="date" id="tanggalSewa" name="tanggalsewa" class="form-control menus" onChange={(e)=> handleTanggal(e)}/>
                        </div>
                        <div class="col-2">
                            <select class="form-select " aria-label="Default select example" name="time"id="waktuJemput" >
                                                    <option selected disabled>Pilih Waktu</option>
                                                    <option value="08:00">08:00 &emsp;&emsp; WIB</option>
                                                    <option value="09:00">09:00 &emsp;&emsp; WIB</option>
                                                    <option value="10:00">10:00 &emsp;&emsp; WIB</option>
                                                    <option value="11:00">11:00 &emsp;&emsp; WIB</option>
                                                    <option value="12:00">12:00 &emsp;&emsp; WIB</option>
                                                </select>
                        </div>

                        
                        <div class="col-3">
                            <input type="number" id="jumlah" name="fname" class="form-control menus" onChange={(e)=> handlekapasitas(e)}/>
                        </div>
                        <div class="col-2 text-start">
                            <button type="button" id="submitData" class="btn btn-success">Cari Mobil</button>
                        </div>
                    </div>
                </div>
            </div>
    
        </section>
        
        <section id="sewa">
            <div class="" id="startCardSewa">
                <div class="container">
                    <div class="row mt-4 card-sewa" id="cars-container">
                        
                
                    {data1.map((item) => 
                            <div class="col-4 ">
                                <div class="card">
                                    <img src={item.image.replace("./" , "https://res.cloudinary.com/dcxjkixo3/image/upload/v1653625106/")} class="card-img-top mobil-gan" alt="..." width/>
                                    
                                    <div class="card-body">
                                        <p class="card-text">Nama/Tipe Mobil</p>
                                        <strong>
                                            <p>20000/ hari</p>
                                        </strong>
                                        <p> deskripsi </p>
                                        <ul class="m-0 p-0">
                                            <li><img src="./image/fi_users.png" alt=""/>kapasitas</li>
                                            <li style={{width: '140px'}}><img src="./image/fi_settings.png" alt=""/>transmisi</li>
                                            <li><img src="./image/fi_calendar.png" alt=""/>tahun</li>
                                        </ul>
                                        <div class="d-grid gap-2 mt-3 kotak-tombol">
                                            <button class="btn btn-success ukuran-tombol" type="button">Pilih Mobil</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         )} 









                     
                    </div>
                </div>
            </div>
    



        </section>



    
    </main>
            
            </>
        </div>
    );
}

export default Cars;


