import React, {Component} from 'react';
import mobil from '../asset/image/img_car.png';
import price from '../asset/image/icon_price.png'
import complete from  '../asset/image/icon_complete.png'
import twentyfour from '../asset/image/icon_24hrs.png'
import profesional from '../asset/image/icon_professional.png'
import cewe from '../asset/image/img_service.png'
import groupcheck from '../asset/image/Group_53.png'
import orang from '../asset/image/orang.png'
import orangdua from '../asset/image/orang_2.png'
import rate from '../asset/image/Rate.png'
import facebook from '../asset/image/icon_facebook.png'
import instagram from '../asset/image/icon_instagram.png'
import twitter from '../asset/image/icon_twitter.png'
import mail from '../asset/image/icon_mail.png'
import twitch from '../asset/image/icon_twitch.png'
import logo1 from '../asset/image/logo_(1).png'

class Home extends Component{
    
    // constructor(props){
    //     super(props)
    //     this.state = {
    //         list_data_post : [],
    //         list_name_column: []
    //     }
    // }

    // componentDidMount(){
    //     fetch("https://jsonplaceholder.typicode.com/posts").then((res)=>res.json())
    //     .then ((data)=>{
    //         this.setState({
    //             list_data_post:data,
    //             list_name_column:Object.keys(data[0])
    //         })
    //     })
    // }
    render(){
        return (
            <>
                <header>
        

        <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor:'#F1F3FF'}}>
            <div className="container-fluid">
              <a className="navbar-brand" href="#" style={{ backgroundColor:'black', marginLeft: '5%'}}>Navbar</a>
              <button id="tombol-burger"className="btn " type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"><span className="navbar-toggler-icon"></span></button>

            <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div className="offcanvas-header">
                
                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div className="offcanvas-body">
                    <ul>
                        <li><a className="cepet" href="#best-car"> Our Service</a></li>
                        <li><a className="cepet" href="#why-us">Why Us</a></li>
                        <li><a  className="cepet" href="#testimonial">Testimonial</a></li>
                        <li><a className="cepet"href="#faq" >FAQ</a></li>
                        <li><button type="button" className="btn btn-success">Register </button></li>
    
                    </ul>
                </div>
            </div>
            </div>
        </nav>

    </header>
    <main style={{width: '100%'}} >
    <section class="desc">
            
            <div class="container-fluid kotak-deskripsi" >
                <div class="row atas"  >
                    <div class="col-lg-6 col-12 justify-center area-deskripsi" >

                        <div class="teks-deksripsi" >
                            <h1 class="h1 desk1">Sewa Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                            <h3 class="h3 mt-3 desk2">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. 
                                Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</h3>
                        </div>
                        <div class="row">

                            <div class="col-md-7 rent">
                                <a href="/cars"><button type="button" class="btn btn-success" id="sewa">Mulai Sewa Mobil </button></a>
                                
                                
                            </div>
                           
                
                
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-6 col-12 justify-content-center area-gambar">
                       
                        <img src={mobil} class="img-fluid float-end area-mobil" />
                       
                    </div>
                    

                </div>
                
                
            </div>  

            
        

        
        


    </section>
    
    <section class="best-car" id="best-car">
            <div class="container-fluid terbaik" >
                <div class="row">
                    <div class="col-lg-6 col-12 justify-content-center area-foto-cewe ">
                         
                        <img src={cewe} class="img-fluid float-start gambar-cewe " />
                       
                    </div>

                    <div class="col-lg-6 col-12 justify-content-center area-best" >
                        <h3 id="best">Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                        <h5 id="lokasi">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</h5>
                        <ul>
                           <li style={{paddingTop:'16px'}}><img src={groupcheck} style={{paddingRight:'16px'}} />Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                            <li style={{paddingTop:'16px'}}><img src={groupcheck} style={{paddingRight:'16px'}}/>Sewa Mobil Lepas Kunci di Bali 24 Jam </li>
                            <li style={{paddingTop:'16px'}}><img src={groupcheck} style={{paddingRight:'16px'}} />Sewa Mobil Jangka Panjang Bulanan</li>
                            <li style={{paddingTop:'16px'}}><img src={groupcheck} style={{paddingRight:'16px'}} />Gratis Antar - Jemput Mobil di Bandara</li>
                            <li style={{paddingTop:'16px'}}><img src={groupcheck} style={{paddingRight:'16px'}} />Layanan Airport Transfer / Drop In Out</li>
                            
                            
        
                        </ul>
                        
                       
                    </div>


                </div>
            
            
            
            
            
            
                <div id="why-us"></div>
            </div>
            
            <div class="container-fluid why-us"  >
            
                    <h2 class="heading">
                        Why Us?
                    </h2>
                    <p class="paragraf-why-us" >Mengapa harus pilih Binar Car Rental?</p>
                    <div class="row" id="kartu">
                        <div class="col-lg-3 col-md-3 col-12 mt-4">
                            <div class="card border batas ">
                                <div class="card-body" style={{border: '1px solid #D0D0D0'}}  >
                                    <img src={complete} />
                                    <h2 class="card-text card-heading pt-3">Mobil Lengkap</h2>
                                    <p class="card-text card-paragraf pt-1">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-12 mt-4">
                            <div class="card border batas">
                                <div class="card-body"style={{border: '1px solid #D0D0D0'}} >
                                    <img src={price}/> 
                                    <h2 class="card-text card-heading pt-3">Harga Murah</h2>
                                    <p class="card-text card-paragraf pt-1">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-12 mt-4">
                            <div class="card border batas">
                                <div class="card-body" style={{border: '1px solid #D0D0D0'}}>
                                    <img src={twentyfour} />
                                    <h2 class="card-text card-heading pt-3">Layanan 24 Jam</h2>
                                    <p class="card-text card-paragraf pt-1">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-12 mt-4">
                            <div class="card border batas" >
                                <div class="card-body" style={{border: '1px solid #D0D0D0'}}>
                                    <img src={profesional} />
                                    <h2 class="card-text card-heading pt-3">Sopir Profesional</h2>
                                    <p class="card-text card-paragraf pt-1">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>

                        
                    
                    </div>
                
        
            </div>


        </section>

        <div id="carouselExampleControls" class="carousel slide" style ={{marginTop: '120px'}}data-bs-ride="carousel">
            <h3 style={{textAlign:'center'}}>Testimonial</h3>
            <h6 style={{textAlign:'center',marginTop: '16px'}}>Berbagai review positif dari pelanggan kami</h6>
            <div class="carousel-inner">

              <div class="carousel-item active" id="slide-pertama">
                    <div class="container-fluid" style={{marginTop:'100px'}}>
                        
                        <div class="row">
                            <div class=" col-4"  id ="item1-1" >
                                <div class=" card border test_1">
                                    <div class="card-body test_1__content ">
                                        <div class="col-4 muka">
                                            <img src= {orang} alt=" " />
                                        </div>
                                        <div class="col-6 position-absolute" style={{marginLeft: '150px',marginTop: '50px'}}>
                                            <img src={rate}alt=" " class="rate "/>
                                            <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                 sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                        </div>
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
                            <div class=" col-4" id ="item1-2">
                                <div class=" card border test_1">
                                    <div class="card-body test_1__content " >
                                        <div class="row area-carosel-konten">
                                            <div class="col-lg-4 col-12 muka" >
                                                <img src={orang} alt=" " />
                                            </div>
                                            <div class="col-lg-6 col-12  teks-carosel" >
                                                <img src={rate}alt=" " class="rate "/>
                                                <p class="paragraf mt-3 text-secondary teks-carosel" >“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                     sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="paragraf mt-2 "style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                        
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
            
                            <div class=" col-4" id ="item1-3" >
                                <div class=" card border test_1">
                                    <div class="card-body test_1__content " >
                                        <div class="col-4 muka"  >
                                            <img src={orangdua} alt=" " />
                                        </div>
                                        <div class="col-6 position-absolute" style={{marginLeft: '150px',marginTop: '50px'}}>
                                            <img src= {rate} alt=" " class="rate "/>
                                            <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                 sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                        </div>
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
                        </div>
                
                
                    </div>
                
                </div>

              <div class="carousel-item" id="slide-kedua">

                    <div class="container-fluid" style={{marginTop:'100px'}}>
                        
                        <div class="row">
                            <div class=" col-4"  id ="item2-1">
                               

                                <div class=" card border test_1">
                                    <div class="card-body test_1__content " >
                                        
                                        <div class="col-lg-4 col-12 muka" >
                                            <img src={orang} alt=" " />
                                        </div>
                                        <div class="col-lg-6 col-12 position-absolute " style={{marginLeft: '150px',marginTop: '50px'}}>
                                            <img src={rate} alt=" " class="rate "/>
                                            <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                 sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                        </div>
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
                            <div class=" col-4 "  id ="item2-2" >
                                <div class=" card border test_1">
                                    <div class="card-body test_1__content " >
                                        <div class="row area-carosel-konten">
                                            <div class="col-lg-4 col-12 muka">
                                                <img src={orangdua} alt=" " />
                                            </div>
                                            <div class="col-lg-6 col-12  area-teks-carosel">
                                                <img src={rate}alt=" " class="rate "/>
                                                <p class="paragraf mt-3 text-secondary teks-carosel" >“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                     sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="paragraf mt-2 "style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                            </div>


                                        </div>
                                       
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
            
                            <div class=" col-4"  id ="item2-3"style={{ paddingLeft:'300px', overflow: 'hidden'}}>
                                 <div class=" card border test_1">
                                    <div class="card-body test_1__content " >
                                        <div class="col-4 muka">
                                            <img src={orang} alt=" " />
                                        </div>
                                        <div class="col-6 position-absolute" style={{marginLeft: '150px',marginTop: '50px'}}>
                                            <img src={rate} alt=" " class="rate "/>
                                            <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                 sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                        </div>
            
            
            
                                    </div>
                                    
                                </div>
                            </div>
            
            
                        </div>
                
                
                    </div>

              </div>
              <div class="carousel-item" id="slide-ketiga">

                <div class="container-fluid" style={{marginTop:'100px'}}>
                        
                    <div class="row">
                        <div class=" col-4" id ="item3-1" style={{marginLeft: '-260px' }}>
                            <div class=" card border test_1">
                                <div class="card-body test_1__content " >
                                    <div class="col-4 muka"  >
                                        <img src={orangdua} alt=" " />
                                    </div>
                                    <div class="col-6 position-absolute " style={{marginLeft: '150px',marginTop: '50px'}}>
                                        <img src={rate}alt=" " class="rate "/>
                                        <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                             sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                        <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                    </div>
        
        
        
                                </div>
                                
                            </div>

                            
                        </div>
        
        
                        <div class=" col-4" id ="item3-2" >
                            <div class=" card border test_1">
                                <div class="card-body test_1__content ">

                                    <div class="row area-carosel-konten">
                                        <div class="col-lg-4 col-12 muka">
                                            <img src={orang} alt=" " />
                                        </div>
                                        <div class="col-lg-6 col-12  area-teks-carosel" >
                                            <img src={rate} alt=" " class="rate"/>
                                            <p class="paragraf mt-3 text-secondary teks-carosel">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                 sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                            <p class="paragraf mt-2 " style={{fontWeight: '600' }}>John Dee 32, Bromo</p>
                                        </div>


                                    </div>
                                    
        
        
        
                                </div>
                                
                            </div>
                        </div>
        
        
        
                        <div class=" col-4"  id ="item3-3" style={{ paddingLeft:'300px', overflow: 'hidden'}}>
                             <div class=" card border test_1">
                                <div class="card-body test_1__content " >
                                    <div class="col-4 muka" >
                                        <img src={orang} alt=" " />
                                    </div>
                                    <div class="col-6 position-absolute" style={{marginLeft: '150px',marginTop: '50px'}}>
                                        <img src={rate} alt=" " class="rate "/>
                                        <p class="paragraf mt-3 text-secondary" style={{width: '450px',
                                            height: '80px'}}>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                             sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                        <p class="paragraf mt-2 "  style={{fontWeight: 600 }}>John Dee 32, Bromo</p>
                                    </div>
        
        
        
                                </div>
                                
                            </div>
                        </div>
        
        
                    </div>
            
            
                </div>
              </div>
            </div>


            
        </div>
      

      
        <div class="btn-carousel" style={{marginTop: '30px'}}>
            <button class="carousel-control btn btn-outline-success btn-circle btn-xl mr-3 " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <i class="fas fa-chevron-left"></i>
                
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control btn btn-outline-success btn-circle btn-xl mr-3" style={{marginLeft: '20px'}} type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <i class="fas fa-chevron-right"></i>
               
                <span class="visually-hidden">Next</span>
            </button>

        </div>


        <section class="beli">
            <div class="container-fluid" style={{marginTop:'120px'}}>
                <div class="row">
                    <div class="kotak" style={{width: '1168px',height: '326px'}}>
                        <h1 style={{textAlign: 'center',color: '#FFFFFF',marginTop: '50px'}}>Sewa Mobil di (Lokasimu) Sekarang</h1>
                        <h6 style={{margin:'auto',textAlign: 'center', maxWidth: '400px' ,color: '#FFFFFF'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h6>
                        <button type="button" id="sewa-mobil" class="btn btn-success" >Mulai Sewa Mobil</button>


                    </div>
                   


                </div>


            </div>

        </section>
        <div class="container-fluid" style={{marginTop: '120px'}}>
            <div class="row">

                <div class="col-lg-6 col-12" id="faq">

                    <h1>
                        Frequently Asked Question
                    </h1>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

                </div>

                <div class="col-lg-6 col-12">
                    <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                              Apa Saja yang dibutuhkan ?
                            </button>
                          </h2>
                          <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias temporibus tenetur beatae quisquam perspiciatis, consectetur deserunt amet et neque voluptas minima vel necessitatibus odio! Amet sunt officiis quos optio mollitia.</div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                Berapa hari minimal sewa mobil lepas kunci?
                            </button>
                          </h2>
                          <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">PLorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias temporibus tenetur beatae quisquam perspiciatis, consectetur deserunt amet et neque voluptas minima vel necessitatibus odio! Amet sunt officiis quos optio mollitia.</div>
                          </div>
                        </div>



                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                    Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias temporibus tenetur beatae quisquam perspiciatis, consectetur deserunt amet et neque voluptas minima vel necessitatibus odio! Amet sunt officiis quos optio mollitia.</div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingfour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsefour" aria-expanded="false" aria-controls="flush-collapsefour">
                                    Apakah Ada biaya antar-jemput? 
                                </button>
                            </h2>
                            <div id="flush-collapsefour" class="accordion-collapse collapse" aria-labelledby="flush-headingfour" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias temporibus tenetur beatae quisquam perspiciatis, consectetur deserunt amet et neque voluptas minima vel necessitatibus odio! Amet sunt officiis quos optio mollitia.</div>
                            </div>
                        </div>      

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingfive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsefive" aria-expanded="false" aria-controls="flush-collapsefive">
                                    Bagaimana jika terjadi kecelakaan ?
                                </button>
                            </h2>
                            <div id="flush-collapsefive" class="accordion-collapse collapse" aria-labelledby="flush-headingfive" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias temporibus tenetur beatae quisquam perspiciatis, consectetur deserunt amet et neque voluptas minima vel necessitatibus odio! Amet sunt officiis quos optio mollitia.</div>
                            </div>
                        </div>
      

                        



                        
                    </div>
                        


                      

                     
                </div>
            </div>
        </div>
        
    </main>
    <footer>
        <div class="container-fluid" id="footer" >
            <div class="row area-footer">
                <div class="col-lg-3 col-12">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div class="col-lg-3 col-12">
                    <li style={{marginBottom: '20px'}}><a class="cepet" href="#best-car"> Our Service</a></li>
                    <li style={{marginBottom: '20px'}}><a class="cepet" href="#why-us">Why Us</a></li>
                    <li style={{marginBottom: '20px'}}><a  class="cepet" href="#testimonial">Testimonial</a></li>
                    <li style={{marginBottom: '20px'}}><a class="cepet" ></a>FAQ</li>
                    



                </div>
                <div class="col-lg-3 col-12">
                    <p>Connect With Us</p>
                    <img src={facebook}/>
                    <img src={instagram}/>
                    <img src={twitter}/>
                    <img src={mail}/>
                    <img src={twitch}/>

                </div>
                <div class="col-lg-3 col-12">
                    <p>Copyright Binar 2022</p>
                    <img  src={logo1}/>


                </div>
            </div>

        </div>
        <h1></h1>

    </footer>
            
            </>
            
        );
    }
}



export default Home