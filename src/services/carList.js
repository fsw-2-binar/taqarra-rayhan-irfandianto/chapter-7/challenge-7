import axios from 'axios';

const url = 'https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json';

export async function carList() {
    try {
        return await axios.get(url);
    } catch (error) {
        console.log(error);   
    }
}