import React,{Component} from "react";

class Table extends Component {
    render(){

        return (
            <>
         <h1  >{this.props.tableName}</h1>
         <table class="table">
            <thead>
                <tr>
                        <th scope="row">no</th>
                            {this.props.list_Column.map((item,index)=>
                        <th scope="row">{item}</th>
                        )}
                    </tr>
            </thead>
                
                {this.props.list_data.map((row,index)=>
                <>
                    <tr >
                        <th scope="row">{index+1}</th>
                        
                    
                        {this.props.list_Column.map((item,index)=>
                            <td >{row[item]}</td>
                        )}
                    </tr>

                
                </>
                        
                        // <td key={index2}>{item}</td>
                )}
           
            
         </table>
        
        </>
        )
            
      
        
       
        
    }
}

export default Table