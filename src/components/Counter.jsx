import React, {Component} from 'react';

class Counter extends Component{
    constructor(props){

        super(props);
        this.state= {
            counter : 0,
            record:[]
        }
    }

    handleDecrease = ()=>{
        
        this.setState  ({
            counter : this.state.counter - 1 
        })
        console.log(this.state.counter)
    }


    handleIncrease = ()=>{
        this.setState  ({
            counter : this.state.counter + 1 
        })
    }
    render(){
        return (
            <div>
                <h1>
                    {this.state.counter}
                </h1>
                <button onClick={()=> this.handleIncrease()}>Increase</button>
                <button onClick={()=> this.handleDecrease()}>decrease</button>
            </div>
        );
    }
}



export default Counter